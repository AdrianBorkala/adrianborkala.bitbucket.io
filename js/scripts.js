// Lazy load certain images to improve performance

document.addEventListener("DOMContentLoaded", function() {
    var lazyloadImages;    
  
    if ("IntersectionObserver" in window) {
      lazyloadImages = document.querySelectorAll(".lazy");
      var imageObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            var image = entry.target;
            image.src = image.dataset.src;
            image.classList.remove("lazy");
            imageObserver.unobserve(image);
          }
        });
      });
  
      lazyloadImages.forEach(function(image) {
        imageObserver.observe(image);
      });
    } else {  
      var lazyloadThrottleTimeout;
      lazyloadImages = document.querySelectorAll(".lazy");
      
      function lazyload () {
        if(lazyloadThrottleTimeout) {
          clearTimeout(lazyloadThrottleTimeout);
        }    
  
        lazyloadThrottleTimeout = setTimeout(function() {
          var scrollTop = window.pageYOffset;
          lazyloadImages.forEach(function(img) {
              if(img.offsetTop < (window.innerHeight + scrollTop)) {
                img.src = img.dataset.src;
                img.classList.remove('lazy');
              }
          });
          if(lazyloadImages.length == 0) { 
            document.removeEventListener("scroll", lazyload);
            window.removeEventListener("resize", lazyload);
            window.removeEventListener("orientationChange", lazyload);
          }
        }, 600);
      }
  
      document.addEventListener("scroll", lazyload);
      window.addEventListener("resize", lazyload);
      window.addEventListener("orientationChange", lazyload);
    }
  });  

// On small devices, show the menu when menu button is pressed

$('.menu-button').click(function(){
    $('.nav').addClass('open');
});

// On small devices, hide the menu when close button is pressed

$('.close-button').click(function(){
    $('.nav').removeClass('open');
});

// List of recent projects

let recentProjects =[
	{
		projectName:"Project 1",
		projectImgURL:"images/project-example-image-1.jpeg",
		projectDesc:"Short description of the project 1",
        projectType:"Commercial"
	},{
		projectName:"Project 2",
		projectImgURL:"images/project-example-image-2.jpeg",
		projectDesc:"Short description of the project 2",
        projectType:"Commercial"
	},{
		projectName:"Project 3",
		projectImgURL:"images/project-example-image-3.jpeg",
		projectDesc:"Short description of the project 3",
        projectType:"Commercial"
    },{
		projectName:"Project 4",
		projectImgURL:"images/project-example-image-4.jpeg",
		projectDesc:"Short description of the project 4",
        projectType:"Residential"
	},{
		projectName:"Project 5",
		projectImgURL:"images/project-example-image-5.jpeg",
		projectDesc:"Short description of the project 5",
        projectType:"Residential"
	},{
		projectName:"Project 6",
		projectImgURL:"images/project-example-image-6.jpeg",
		projectDesc:"Short description of the project 6",
        projectType:"Residential"
    }
];

// Declare 2 variables to store recent projects split by their type

let recentComProjects;
let recentResProjects;

// Find and store recent Commercial projects 
// I used the following q & a to learn how to only skip past items I didn't want from my initial object: https://stackoverflow.com/questions/24806772/how-to-skip-over-an-element-in-map

function showRecentCom() {

    recentComProjects = recentProjects.filter(function(proj) {
        if (proj.projectType !== "Commercial") {
          return false; // skip
        }
        return true;
    }).map(function(proj) { 
        return {
            projectName: proj.projectName,
            projectImgURL: proj.projectImgURL,
            projectDesc: proj.projectDesc
        }
    });

}

// Find and store recent Residential projects 
// I used the following q & a to learn how to only skip past items I didn't want from my initial object: https://stackoverflow.com/questions/24806772/how-to-skip-over-an-element-in-map

function showRecentRes() {

    recentResProjects = recentProjects.filter(function(proj) {
        if (proj.projectType !== "Residential") {
          return false; // skip
        }
        return true;
    }).map(function(proj) { 
        return {
            projectName: proj.projectName,
            projectImgURL: proj.projectImgURL,
            projectDesc: proj.projectDesc
        }
    });

}

// Run functions to prepare recent projects information to display

showRecentCom();
showRecentRes();

// Listen for a button click and display the appropriate set of projects
// Bind the click event for the dynamically created content to it's parent div so that we can do something when it's clicked. Learnt about this here: https://stackoverflow.com/questions/203198/event-binding-on-dynamically-created-elements

$('.project-question').on('click', '.button-projects', function(){
    // Determine if the Commerical or Residential button was clicked so the correct set of projects are shown
    var projectType = $(this).attr('data-projtype');

    var projectSearch = (projectType === 'commercial') ? recentComProjects :
    recentResProjects;

    console.log(projectSearch);

    $('.project-question').addClass('move');

    // Asked a question on Stackoverflow to work out a way to slide content off screen and move the content whilst dynamically updating it and then bringing it back on as though it was a new div. Here's my question and the answer I used to make the following work: https://stackoverflow.com/questions/68308889/how-can-i-transition-content-off-screen-in-one-direction-fading-it-out-update/68309384#68309384

    setTimeout(()=>{
        $('.project-question').removeClass('move')
        $('.project-question').addClass('right')
        $('.project-question').html(`
        <h2>Our recent ${projectType} projects</h2>
        <div class="projectsSlider">
        `);

        for(let i = 0; i < projectSearch.length; i++) {

            let project = `
                <div class="project-example">
                    <img src="${projectSearch[i].projectImgURL}" alt="">
                    <div class="proj-deets">
                        <h3>${projectSearch[i].projectName}</h3>
                        <p>${projectSearch[i].projectDesc}</p>
                    </div>
                </div>
            `
            $('.projectsSlider').append(project);
        }

        $('.project-question').append(`
        </div>
        <div class="row">
            <div class="col back-more">
                <a href="#0" class="button-secondary back-to-start" role="button" data-wherenext="back">back to project choices</a>
                <a href="#0" class="button-secondary" role="button" data-wherenext="back">See more like these</a>
            </div>
        </div>
        `
        );

        // Initiate and show the slick sliders only after the element this sits in is visible

        $('.projectsSlider').slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1160,
                    settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                    }
                }
                ]
        });
        $('.projectsSlider').show();
    }, 600);

    setTimeout(()=>{
        $('.project-question').addClass('left');
        $('.project-question').removeClass('right');
    }, 1200);
    
});

// Bind the click event for the dynamically created content to it's parent div so that we can do something when it's clicked. Learnt about this here: https://stackoverflow.com/questions/203198/event-binding-on-dynamically-created-elements

$('.project-question').on('click', '.back-to-start', function(){
    console.log('button pressed');
    $('.project-question').removeClass('left');
    $('.project-question').addClass('move-back');

    setTimeout(()=>{
        $('.project-question').removeClass('move-back')
        $('.project-question').addClass('left-back')
        $('.project-question').html(`
        <h2 class="proj-heading">Do you have a project in mind?</h2>
        <p>If you're looking for inspiration, a quote, or just to see our portfolio, let us get you to the right place based on the type of project you're interested in</p>
        <ul>
            <li><a href="#0" class="button button-projects" role="button" data-projtype="commercial">Commercial&nbsp;project</a></li>
            <li><a href="#0" class="button button-projects" role="button" data-projtype="residential">Residential&nbsp;project</a></li>
        </ul>
        `);
    }, 600);

    setTimeout(()=>{
        $('.project-question').addClass('left');
        $('.project-question').removeClass('left-back');
    }, 1200)

    setTimeout(()=>{
        $('.project-question').removeClass('left');
    }, 1600)
});